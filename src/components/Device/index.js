import styled from 'styled-components'
import main from 'src/pages/Dashboard/main.json'

const BoxShowSvg = styled("div")`
  -webkit-box-shadow: 0 5px 11px 0 rgb(0 0 0 / 18%), 0 4px 15px 0 rgb(0 0 0 / 15%) !important;
  box-shadow: 0 5px 11px 0 rgb(0 0 0 / 18%), 0 4px 15px 0 rgb(0 0 0 / 15%) !important;
  heigth: 100%;
  margin: 20px;
  display: flex;
  align-content: center;
  justify-content: center;
  align-items: center;
  flex-wrap: nowrap;
  flex-direction: row;
`

export default function Dashboard() {

  return (
    <div>
      <BoxShowSvg dangerouslySetInnerHTML={{__html: main.hmi.views[0].svgcontent}}></BoxShowSvg>
    </div>
  )
}