import React from 'react';
import styled from 'styled-components'
import { makeStyles, withStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import CircularProgress from '@material-ui/core/CircularProgress'
import Backdrop from '@material-ui/core/Backdrop'
import TextBox from 'src/components/TextBox';
import Button from '@material-ui/core/Button'
import NativeSelect from '@material-ui/core/NativeSelect'
import InputBase from '@material-ui/core/InputBase'
// import SearchIcon from 'src/components/Table/Search';

import config from 'src/apis/config'

import TimePicker from 'src/components/TimePicker'
import parseDate from 'src/commons/parseDate';
import parseTime from 'src/commons/parseTime';
import parseTimetoFulltime from 'src/commons/parseTimetoFulltime';
import parseDateTime from 'src/commons/parseDateTime'

import * as mqtt from 'paho-mqtt';
import moment from 'moment'
import {
  Search
} from "src/stores/actions"

import getLog from 'src/apis/getLog'
import getDevice from 'src/apis/getDevice'

const CustomeSelectBox = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    width: "100px",
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '13px 10px 13px 16px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      // borderColor: '#80bdff',
      // boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase);

const columns = [
  { id: 'date', label: 'Date&Time', align: 'left',},
  { id: 'name', label: 'Remote Name', align: 'left',},
  { id: 'Text', label: 'Text', align: 'left',},
  { id: 'Value', label: 'Value', align: 'left',},
  { id: 'Status', label: 'Status', align: 'left',},
]

const BoxTableLog = styled("div")`
  -webkit-box-shadow: 0 5px 11px 0 rgb(0 0 0 / 18%), 0 4px 15px 0 rgb(0 0 0 / 15%) !important;
  box-shadow: 0 5px 11px 0 rgb(0 0 0 / 18%), 0 4px 15px 0 rgb(0 0 0 / 15%) !important;
  heigth: 100%;
  margin: 20px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`

const BoxTableLogHeader = styled("div")`
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  padding: 20px;
  font-size: 22px;
  font-weight: 100;
  display: flex;
  justify-content: space-between;
`
const BoxTableLogBody = styled("div")`
  padding: 20px;
`

const Divided = styled("div")`
  width: 2px;
  height: 50px;
  border-right: 1px solid rgba(0,0,0,0.1);
  margin-left: 10px;
`

const TageSearch = styled('div')`
  height: 50px;
  font-size: 15px;
  margin-left: 10px;
  display: flex;
  align-content: center;
  justify-content: center;
  align-items: center;
`

const HeaderLeft = styled('div')`
  display: flex;
  align-content: center;
  align-items: center;
`

const HeaderRight = styled('div')`
  display: flex;
  align-content: center;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  padding-top: 20px;
  padding-bottom: 10px;
`

const TextContainer = styled('div')`
display: flex;
`

const RemoteName = styled('div')`
font-size: 16px;
display: flex;
padding-left: 10px;
align-items: center;
align-content: center;
`

const SortForm = styled("div")`
  margin: 0px 10px;
  display: flex;
  justify-content: center;
  align-items: center;
`

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    Height: "100%",
  },
  timeout: {
    backgroundColor: "#fbc2c2",
  },
  backdrop: {
    zIndex: 30001,
    color: '#fff',
  },
  DatePicker: {
    marginLeft: "20px"
  },
  textFieldDate: {
    marginLeft: 10,
    marginRight: 10,
    width: 200,
  },
  textFieldTime: {
    marginLeft: 10,
    marginRight: 10,
    width: 100,
  },
});

export default function TableLog (props) {
  const classes = useStyles();
  const [ store ] = React.useState(props.store)
  const [ remote_name ] = React.useState(props.remotename)
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const [OpenBackdrop, setOpenBackdrop] = React.useState(false);

  const [ isRealTime, setisRealTime ] = React.useState(true);

  const [ TimeStart, setTimeStart ] = React.useState("00:00");
  const [ TimeStop, setTimeStop ] = React.useState("23:59");
  const [ DateStart, setDateStart ] = React.useState("");
  const [ DateStop, setDateStop ] = React.useState("");

  const [ Device, setDevice ] = React.useState(undefined);

  const [ Datalog, setDatalog ] = React.useState(undefined);

  const [ Query, setQuery ] = React.useState({});

  const handleChangePage = (event, newPage) => {
    var qq = Query
    console.log(Number(newPage) * Number(qq.limit))
    qq.offset = Number(newPage) * Number(qq.limit)
    console.log(qq)
    setQuery(qq)
    handleQueryLog(qq)
  };

  const handleChangeRowsPerPage = (event) => {
    var qq = Query
    qq.limit = Number(event.target.value)
    qq.offset = Number(0)
    console.log(qq)
    setQuery(qq)
    handleQueryLog(qq)
  };
  
  const onTimeStart = (value) => {
    setTimeStart(value)
    var initdt = Query
    initdt.time_start = value
    setQuery(Object.assign({}, initdt))
  }

  const onTimeStop = (value) => {
    setTimeStop(value)
    var initdt = Query
    initdt.time_stop = value
    setQuery(Object.assign({}, initdt))
  }

  const updateSVG = (topic, data) => {
        // Pressure
        if (topic === "logger/"+remote_name+"/afv/pressure"){
          document.getElementById("VAL_b565bdee-ba28430a").innerHTML = data.ps
          document.getElementById("VAL_582f988b-1b244b69").innerHTML = moment(new Date((data.ts - (3600 * 7)) * 1000)).format('L HH:mm:ss')
        }
    
        // Flow Active
        if (topic === "logger/"+remote_name+"/afv/flow/active"){
          document.getElementById("VAL_bb71a8cb-00ea43a5").innerHTML = "Active"
          document.getElementById("VAL_2b5e53cd-5c65422c").innerHTML = moment(new Date((data.ts - (3600 * 7)) * 1000)).format('L HH:mm:ss')
        }
    
        // Flow Inactive
        if (topic === "logger/"+remote_name+"/afv/flow/inactive"){
          document.getElementById("VAL_bb71a8cb-00ea43a5").innerHTML = "Inactive"
          document.getElementById("VAL_2b5e53cd-5c65422c").innerHTML = moment(new Date((data.ts - (3600 * 7)) * 1000)).format('L HH:mm:ss')
        }
    
        // Valve Open
        if (topic === "logger/"+remote_name+"/afv/valve/open"){
          document.getElementById("VAL_c9374859-305147ee").innerHTML = "Open"
          document.getElementById("VAL_8606033c-9ee541a1").innerHTML = moment(new Date((data.ts - (3600 * 7)) * 1000)).format('L HH:mm:ss')
          document.getElementById("GSE_8278add1-327f45b5").setAttribute("fill", "#077C07")
        }
    
        // Valve Close
        if (topic === "logger/"+remote_name+"/afv/valve/close"){
          document.getElementById("VAL_c9374859-305147ee").innerHTML = "Close"
          document.getElementById("VAL_8606033c-9ee541a1").innerHTML = moment(new Date((data.ts - (3600 * 7)) * 1000)).format('L HH:mm:ss')
          document.getElementById("GSE_8278add1-327f45b5").setAttribute("fill", "#828882")
        }
    
        // Valve open Timeout
        if (topic === "logger/"+remote_name+"/afv/valve/open/timeout"){
        }
    
        // Valve close Timeout
        if (topic === "logger/"+remote_name+"/afv/valve/close/timeout"){
        }
    
        // Volume
        if (topic === "logger/"+remote_name+"/afv/volume"){
          document.getElementById("VAL_ad46811e-20e54304").innerHTML = data.vo
          document.getElementById("VAL_4b3e7902-487e4319").innerHTML = moment(new Date((data.ts - (3600 * 7)) * 1000)).format('L HH:mm:ss')
        }
  }
  
  const onMessageArrived = (message) => {
    var topic = message.destinationName
    var data = JSON.parse(message.payloadString)
    updateSVG(topic, data)
  }

    // called when the client loses its connection
    const onConnectionLost = (responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log(responseObject.errorMessage);
      }
    }

  React.useEffect(() => {
    const initDate = () => {
      console.log("On init")
      handleQueryDevice()
      var client = new mqtt.Client(config.mqtt, Number(config.port), "__AFV_MQTT_REACT_WEB_" + Math.random().toString().slice(2,10));
      // connect the client
      client.connect({
        onSuccess:onConnect,
        userName : config.user,
        password : config.pass,
      });

      // set callback handlers
      client.onConnectionLost = onConnectionLost;
      client.onMessageArrived = onMessageArrived;

      function onConnect() {
        // Once a connection has been made, make a subscription and send a message.
        console.log("onConnect");
        client.subscribe("logger/"+remote_name+"/afv/pressure");
        client.subscribe("logger/"+remote_name+"/afv/flow/active");
        client.subscribe("logger/"+remote_name+"/afv/flow/inactive");
        client.subscribe("logger/"+remote_name+"/afv/valve/open");
        client.subscribe("logger/"+remote_name+"/afv/valve/open/timeout");
        client.subscribe("logger/"+remote_name+"/afv/valve/close");
        client.subscribe("logger/"+remote_name+"/afv/valve/close/timeout");
        client.subscribe("logger/"+remote_name+"/afv/volume");
      }

      // console.log(client)
      store.dispatch(Search({
        offset: 0,
        limit: 10,
        remote_name: remote_name,
        start: parseTimetoFulltime(1 ,"00:00", "00.000Z"),
        end: parseTimetoFulltime(1 ,"23:59", "59.999Z")
      }))
      const initdt = {
        offset: 0,
        limit: 10,
        sort: -1, 
        remote_name: remote_name,
        date_start: parseDate(parseTimetoFulltime(1 ,"00:00", "00.000Z")),
        time_start: parseTime(parseTimetoFulltime(1 ,"00:00", "00.000Z")),
        date_stop: parseDate(parseTimetoFulltime(1 ,"23:59", "59.999Z")),
        time_stop: parseTime(parseTimetoFulltime(1 ,"23:59", "59.999Z")),
      }
      setQuery(Object.assign({}, initdt))
      setTimeStart( parseTime(parseTimetoFulltime(1 ,"00:00", "00.000Z")) )
      setTimeStop( parseTime(parseTimetoFulltime(1 ,"23:59", "59.999Z")) )
      setDateStart( parseDate(parseTimetoFulltime(1 ,"00:00", "00.000Z")) )
      setDateStop( parseDate(parseTimetoFulltime(1 ,"23:59", "59.999Z")) )
      handleQueryLog(initdt)
    }
    initDate()
  }, [store])

  const handleQueryDevice = async () => {
    let From = new FormData();
    From.append('device_id', remote_name)
    const res = await getDevice(From)
    setDevice(Object.assign({}, res.data))
    if (res.data.result.length > 0){
      // update data
      var data_ = res.data.result[0]
      console.log(data_)
      if (data_.pressure !== undefined){
        var topic = data_.pressure.topic
        var data = data_.pressure.payload
        console.log(topic)
        console.log(data)
        updateSVG(topic, data)
      }
      if (data_.flow !== undefined){
        var topic = res.data.result[0].flow.topic
        var data = res.data.result[0].flow.payload
        updateSVG(topic, data)
      }
      if (data_.valve !== undefined){
        var topic = res.data.result[0].valve.topic
        var data = res.data.result[0].valve.payload
        updateSVG(topic, data)
      }
      if (data_.volume !== undefined){
        var topic = res.data.result[0].volume.topic
        var data = res.data.result[0].volume.payload
        updateSVG(topic, data)
      }
      // var topic = res.data.result[0]
      // updateSVG(topic, data)
    }
  }

  const handleQueryLog = async (data) => {
    console.log(data)
    setOpenBackdrop(true)
    let From = new FormData();
    From.append('device_id', data.remote_name)
    From.append('start_time', `${data.date_start} ${data.time_start}`)
    From.append('stop_time', `${data.date_stop} ${data.time_stop}`)
    From.append('offset', Number(data.offset))
    From.append('limit', Number(data.limit))
    From.append('sort', Number(data.sort))
    const res = await getLog(From);
    setDatalog(res.data)
    setOpenBackdrop(false)
  }

  return (
    <BoxTableLog>
      <BoxTableLogHeader> 
        <HeaderLeft>
          { (isRealTime ? <TextContainer> ข้อมูลย้อนหลัง <RemoteName>[ Remote Name: {remote_name} ] </RemoteName> </TextContainer>: `RealTime [ Remote Name: ${remote_name} ]`) }
          {/* <Divided /> */}
        </HeaderLeft>
      </BoxTableLogHeader>

      <HeaderRight>
        <TageSearch>เวลาเริ่มค้นหา</TageSearch>
            <TextBox
              type="date"
              value={DateStart}
              className={classes.textFieldDate}
              variant="outlined" 
              fullWidth={true}
              autoComplete='off'
              format="yyyy-MM-dd"
              onChange={(e) => {
                setDateStart(e.target.value)
                var initdt = Query
                initdt.date_start = e.target.value
                setQuery(Object.assign({}, initdt))
              }}
            />

            <TextBox
              id="time"
              type="time"
              value={TimeStart}
              className={classes.textFieldDates}
              variant="outlined" 
              InputLabelProps={{
                shrink: true,
              }}
              inputProps={{
                step: 300, // 5 min
              }}
              onChange={(e) => {
                onTimeStart(e.target.value)
              }}
            />

            {/* <TimePicker onTime={onTimeStart} init={TimeStart} tag="start-time-form-"/> */}

            <TageSearch>เวลาสิ้นสุดค้นหา</TageSearch>
            <TextBox
              type="date"
              value={DateStop}
              className={classes.textFieldDate}
              variant="outlined" 
              labelwidth={0}
              placeholder="กรอกชื่อ" 
              format="yyyy-MM-dd"
              fullWidth={true}
              autoComplete='off'
              onChange={(e) => {
                setDateStop(e.target.value)
                var initdt = Query
                initdt.date_stop = e.target.valu
                setQuery(Object.assign({}, initdt))
              }}
            />

            <TextBox
              id="time"
              type="time"
              value={TimeStop}
              className={classes.textFieldDates}
              variant="outlined" 
              InputLabelProps={{
                shrink: true,
              }}
              inputProps={{
                step: 300, // 5 min
              }}
              onChange={(e) => {
                onTimeStop(e.target.value)
              }}
            />

            {/* <TimePicker onTime={onTimeStop} init={TimeStop} tag="end-time-form-"/> */}

            {/* <SortForm>
              <TageSearch style={{marginRight: "10px"}}>เรียงลำดับเวลา</TageSearch>
              <NativeSelect
                id="demo-customized-select-native"
                input={<CustomeSelectBox />}
                value={Query.sort}
                onChange={(e) => {
                  var sort = e.target.value
                  var initdt = Query
                  initdt.sort = sort
                  setQuery(Object.assign({}, initdt))
                }}
              >
                <option value={-1}>จากมากไปน้อย</option>
                <option value={1}>จากน้อยไปมาก</option>
              </NativeSelect>
            </SortForm> */}

            <Button variant="contained" color="primary" style={{color: "#000", marginLeft: "20px"}}
              onClick={() => {
                handleQueryLog(Query)
              }}
            >
              ค้นหา
            </Button>
        </HeaderRight>

        { (Datalog !== undefined ? 
          <BoxTableLogBody>
            <Paper className={classes.root}>
              <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow>
                      {columns.map((column) => (
                        <TableCell
                          key={column.id}
                          align={column.align}
                          style={{ fontSize: 18 }}
                        >
                          {column.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {Datalog.message.map((row) => {
                      return (
                        <TableRow role="checkbox" tabIndex={-1} key={row._id} className={(row.status === "Timeout" ? classes.timeout : null)}>
                          <TableCell align={"left"} >
                            {
                              parseDateTime(row.time_utc)
                            }
                          </TableCell>
                          <TableCell align={"left"} >
                            {
                              row.device_id
                            }
                          </TableCell>

                          <TableCell align={"left"} >
                            {
                              row.text
                            }
                          </TableCell>

                          <TableCell align={"left"} >
                            {
                              row.value
                            }
                          </TableCell>

                          <TableCell align={"left"} >
                            {
                              row.status
                            }
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, 50, 100]}
                component="div"
                count={Datalog.length}
                rowsPerPage={Number(Query.limit)}
                page={Number(Query.offset) / Number(Query.limit)}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />
            </Paper>
    
          </BoxTableLogBody>  
        : null) }


      <Backdrop className={classes.backdrop} open={OpenBackdrop}>
        <CircularProgress color="inherit" />
      </Backdrop>

    </BoxTableLog>
  )
}