
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';
import { blue } from '@material-ui/core/colors';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton'


const emails = ['username@gmail.com', 'user02@gmail.com'];
const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
});

function DialogSearch(props) {
  const classes = useStyles();
  const { onClose, selectedValue, open } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  const handleListItemClick = (value) => {
    onClose(value);
  };

  return (
    <Dialog disableBackdropClick={true} maxWidth={"lg"} onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">Set backup account</DialogTitle>
      Hello
    </Dialog>
  );
}

DialogSearch.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};



export default function Search () {

  const [openDialog, setopenDialog] = React.useState(false);

  const handleClickOpenDialog = () => {
    setopenDialog(true);
  };

  const handleCloseDialog = (value) => {
    setopenDialog(false);
    console.log(value)
  };

  return (
    <div>
      <IconButton onClick={() => {
        setopenDialog(true)
      }}>
        <SearchIcon />
      </IconButton>
      <DialogSearch selectedValue={handleClickOpenDialog} open={openDialog} onClose={handleCloseDialog}  />
    </div>
  )
}