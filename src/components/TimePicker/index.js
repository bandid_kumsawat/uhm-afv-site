import React from 'react'

import TextBox from 'src/components/TextBox';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import IconButton from '@material-ui/core/IconButton';

// TimePicker
// sub Component
import Container from './Container'
// Pseudo Style
import "./style.css"
// setting style import TimeStyles from 'src/components/TimePicker/style'
import TimeStyles from './TimeStyle.js'
// time format
import Time from 'src/components/TimePicker/time.json'
// icon
import UpTime from "src/assets/img/timepicker/up.svg";
import DownTime from "src/assets/img/timepicker/down.svg";

const useStyles = makeStyles({
  textFieldTime: {
    marginLeft: 10,
    marginRight: 10,
    width: 100,
  },
});

const CustomeMenu = withStyles({
  paper: {
    boxShadow: "0px 5px 5px -3px rgb(0 0 0 / 20%), 0px 8px 10px 1px rgb(0 0 0 / 14%), 0px 3px 14px 2px rgb(0 0 0 / 12%)"
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'left',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'left',
    }}
    {...props}
  />
));

var t1 = "00"
var t2 = "00"

function TimePickerList(props) {
  const classestime = TimeStyles()
  const {anchorEl , onClose, TimePicketInit, tag} = props;
  
  const [ TimeFrom , setTimeFrom ] = React.useState("00:00")

  const [ HoursFrom, setHoursFrom ] = React.useState("00") // true
  const [ MinuteseFrom, setMinuteseFrom ] = React.useState("00") // false

  const ScorllHoursFrom = React.createRef();
  const ScorllMinuteseFrom = React.createRef();

  React.useEffect(() => {
    const initTime = () => {
      setTimeFrom( TimePicketInit )
      setHoursFrom( TimePicketInit.split(":")[0] )
      setMinuteseFrom( TimePicketInit.split(":")[1] )
      t1 = TimePicketInit.split(":")[0]
      t2 = TimePicketInit.split(":")[1]
    }
    initTime()
  }, [])

  return (
    <div>
      <CustomeMenu
        id="simple-menu"
        keepMounted
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={() => {
          onClose(null, TimeFrom)
        }}
      >
        
        <Container  >

        <div style={{
          display: "flex",
          alignContent: "center",
          alignItems: "center",
          justifyContent: "center",
        }}>

          <div style={{
            width: 40,
            height: 30,
            display: "flex",
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center",
            marginBottom: "10px"
          }}>
            <IconButton onClick={(e) => {
              var tNumber = t1
              var elmnt = document.getElementById("scroll-hours-from-" + tag + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
              elmnt.scrollIntoView();
              if (Number(t1) > 0){
                t1--
                setHoursFrom(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                setTimeFrom(
                  ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                  ":" + 
                  ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                )
              }
            }}>
              <img src={UpTime} alt="UpTime" />
            </IconButton>
          </div>
          <div style={{
            width: 40,
          }}></div>
          <div style={{
            width: 40,
            height: 30,
            display: "flex",
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center",
            marginBottom: "10px"
          }}>
            <IconButton onClick={() => {
              var tNumber = t2
              var elmnt = document.getElementById("scroll-minutese-from-" + tag + ((Number(tNumber) !== 0) ? (Number(tNumber) - 1): Number(tNumber)));
              elmnt.scrollIntoView();
              if (Number(t2) > 0){
                t2--
                setMinuteseFrom(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                setTimeFrom(
                  ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                  ":" + 
                  ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                )

              }
            }}>
              <img src={UpTime} alt="UpTime" />
            </IconButton>
          </div>
        </div>

        <div style={{
          display: "flex"
        }}>
          <div style={{
            height: "200px",
            width: "40px",
            overflow: "scroll",
          }} 
          ref={ScorllHoursFrom}
          className="remove-scroll"
          >
            {
              Time.Hours.map((item, index) => {
                return (
                  <div
                      id={"scroll-hours-from-" + tag + Number(index)}
                      key={index}
                      style={{
                      width: "40px",
                      height: "40px",
                      display: "flex",
                      alignContent: "center",
                      justifyContent: "center",
                    }}
                    >
                      <IconButton 
                        onClick={() => {
                          setHoursFrom(item)
                          t1 = item
                          setTimeFrom(
                            ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                            ":" + 
                            ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                          )
                      }}
                      className={((item === HoursFrom) ? classestime.Select: classestime.Unselect)}
                    >{item}</IconButton>
                  </div>  
                )
              })
            }
          </div>
          <div style={{
            height: "200px",
            width: "40px",
            fontSize: "28px",
            display: "flex",
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center",
          }} >:</div>
          <div style={{
            height: "200px",
            width: "40px",
            overflow: "scroll",
          }} 
          ref={ScorllMinuteseFrom}
          className="remove-scroll"
          >
            {
              Time.Minutese.map((item, index) => {
                return (
                  <div
                    key={index}
                    id={"scroll-minutese-from-" + tag + Number(index)}
                    style={{
                    width: "40px",
                    height: "40px",
                    display: "flex",
                    alignContent: "center",
                    justifyContent: "center",
                  }}>
                    <IconButton 
                      className={((item === MinuteseFrom) ? classestime.Select: classestime.Unselect)} 
                      onClick={() => {
                        setMinuteseFrom(item)
                        t2 = item
                        setTimeFrom(
                          ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                          ":" + 
                          ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                        )
                      }}
                    >{item}</IconButton>
                  </div>  
                )
              })
            }
          </div>
        </div>

        <div style={{
          display: "flex",
          alignContent: "center",
          alignItems: "center",
          justifyContent: "center",
        }}>

          <div style={{
            width: 40,
            height: 30,
            display: "flex",
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center",
            marginTop: "10px"
          }}>
            <IconButton onClick={() => {
              var tNumber = t1
              var elmnt = document.getElementById("scroll-hours-from-" + tag + ((Number(tNumber) !== 23) ? (Number(tNumber) + 1): Number(tNumber)));
              elmnt.scrollIntoView();
              if (Number(t1) < 23){
                t1++
                setHoursFrom(((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()))
                setTimeFrom(
                  ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                  ":" + 
                  ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                )
              }
            }}>
              <img src={DownTime} alt="DownTime" />
            </IconButton>
          </div>
          <div style={{
            width: 40,
          }}></div>
          <div style={{
            width: 40,
            height: 30,
            display: "flex",
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center",
            marginTop: "10px"
          }}>
            <IconButton onClick={() => {
              var tNumber = t2
              var elmnt = document.getElementById("scroll-minutese-from-" + tag + ((Number(tNumber) !== 59) ? (Number(tNumber) + 1): Number(tNumber)));
              elmnt.scrollIntoView();
              if (Number(t2) < 59){
                t2++
                setMinuteseFrom(((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString()))
                setTimeFrom(
                  ((Number(t1) < 10) ? ("0" + Number(t1).toString()): Number(t1).toString()) + 
                  ":" + 
                  ((Number(t2) < 10) ? ("0" + Number(t2).toString()): Number(t2).toString())
                )
              }
            }}>
              <img src={DownTime} alt="DownTime" />
            </IconButton>
          </div>
        </div>

        </Container>
      </CustomeMenu>
    </div>
  );
}

export default function TimePicker (props) {
  const classes = useStyles();
  const classestime = TimeStyles()

  const { onTime, init, tag } = props
  const [anchorEl, setanchorEl] = React.useState(null);


  const onClose_ = (state, time) => {
    onTime(time)
    setanchorEl(state)
  }
  React.useEffect(() => {
    const initValue = () => {

    }
    initValue()
  }, [])

  return (
      <div>
        <TextBox
          className={classes.textFieldTime}
          InputLabelProps={{
            shrink: true,
          }}
          value={init}
          variant="outlined" 
          fullWidth={true}
          autoComplete='off'
          onClick={(event) => {
            setanchorEl(event.currentTarget);
          }}
        >
      </TextBox>
      <TimePickerList anchorEl={anchorEl} onClose={onClose_} TimePicketInit={init} tag={tag}/>
    </div>
  )
}