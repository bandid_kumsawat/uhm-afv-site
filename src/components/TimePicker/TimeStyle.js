import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  Select: {
    borderRadius: "100px",
    backgroundColor: "#01A6E6",
    color: "#fff",
    width: 40,
    height: 40,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 18,
    cursor: "pointer",
    '&:hover': {
      backgroundColor: '#01A6E66',
      color: "#000",
    },
  },
  Unselect: {
    color: "#000",
    width: 40,
    height: 40,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 18,
    cursor: "pointer",
    '&:hover': {
      backgroundColor: '#01A6E66',
      color: "#000",
    },
  }
}));


export default useStyles