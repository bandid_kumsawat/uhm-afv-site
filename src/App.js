import React from 'react'

import Dashboard from 'src/pages/Dashboard'

import { useRoutes } from 'react-router-dom';

import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import './App.css';

import { store } from './stores/store'

const theme = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 450,
      md: 600,
      lg: 900,
      xl: 1200
    }
  },
  palette: {
    primary: {
      main: "#01A6E6",
    },
    secondary: {
      main: "#FFFFFF",
    },
  },
});

function App() {

  const routing = useRoutes([
    {
      path: '/:remote_name',
      element: <Dashboard store={ store } />,
    }, {
      path: "*",
      element: <div className="remote-name-not-found">กรุณาใส่ Remote Name ด้วย</div>
    }
  ]);

  return (
    <MuiThemeProvider theme={theme}>
      { routing }
    </MuiThemeProvider>
  );
}

export default App;
