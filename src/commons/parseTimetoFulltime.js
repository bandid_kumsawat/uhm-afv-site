const addDigiNubmerStr = (snum) => {
  return ((Number(snum) < 10) ? "0" + snum.toString(): snum.toString())
}

/*
  tail = 00.000Z
  tail = 59.999Z
*/

export default function parseTimetoFulltime (day, time, tail) {
  var Full = new Date()
  var h = time.split(":")[0]
  var m = time.split(":")[1]
  Full.setDate(Full.getDate() - day)
  Full.setHours(h)
  Full.setMinutes(m)
  return `${addDigiNubmerStr(Full.getFullYear())}-${addDigiNubmerStr(Full.getMonth() + 1 )}-${addDigiNubmerStr(Full.getDate() )}T${addDigiNubmerStr(Full.getHours()  )}:${addDigiNubmerStr(Full.getMinutes())}:${tail}`
} 