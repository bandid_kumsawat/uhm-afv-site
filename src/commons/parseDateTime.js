const addDigiNubmerStr = (snum) => {
  return ((Number(snum) < 10) ? "0" + snum.toString(): snum.toString())
}

/*
  tail = 00.000Z
  tail = 59.999Z
*/

export default function parseDateTime (time) {
  var Full = new Date(time)
  return `${addDigiNubmerStr(Full.getFullYear())}-${addDigiNubmerStr(Full.getMonth() + 1 )}-${addDigiNubmerStr(Full.getDate() )} ${addDigiNubmerStr(Full.getHours()  )}:${addDigiNubmerStr(Full.getMinutes())}:${addDigiNubmerStr(Full.getSeconds())}`
} 