const addDigiNubmerStr = (snum) => {
  return ((Number(snum) < 10) ? "0" + snum.toString(): snum.toString())
}

export default function parseDate (time) {
  var str = ''
  if (time !== ""){
    var dd = new Date(time)
    str =  dd.getFullYear() + "-" + addDigiNubmerStr(dd.getMonth() + 1)  + "-"+ addDigiNubmerStr(dd.getDate())
  } else  {
    str = time
  }
  return str
}