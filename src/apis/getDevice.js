
import axios from 'axios'
import sconfig from 'src/apis/config'

// eslint-disable-next-line
export default (data) => {
  return axios({
    method: 'post',
    url: sconfig.url +'/device',
    data : data
  })
}