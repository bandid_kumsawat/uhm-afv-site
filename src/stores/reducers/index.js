import initialState from './state'

//eslint-disable-next-line
export default (state = initialState, action) => {
  switch (action.type) {
    case 'Logs':
      return Object.assign({}, state, {
        Logs: action.payload
      })
    case 'Search':
      return Object.assign({}, state, {
        Search: action.payload
      })
    default:
      return state
  }
}
