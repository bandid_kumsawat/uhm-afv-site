import React from 'react'
import { useParams } from 'react-router-dom';
import styled from 'styled-components'

//Component 
import Device from 'src/components/Device'
import Table from 'src/components/Table'

import parseTimetoFulltime from 'src/commons/parseTimetoFulltime'

import {
  Search
} from 'src/stores/actions'

const Header = styled("div")`
  //width: 100%;
  font-weight: 300;
  font-size: 18px;
  padding: 0.75rem 1.25rem;
  margin-bottom: 0;
  background-color: rgba(0, 0, 0, 0.03);
  border-bottom: 1px solid rgba(0, 0, 0, 0.125);
  color: #212529;
`
const Container = styled("div")`
  width: 100%;
`

export default function Dashboard(props) {
  let { remote_name } = useParams();
  const [ store ] = React.useState(props.store)

  React.useEffect(() => {

    setInterval(() => {
      // console.log(remote_name)
    }, 1000);

  }, [store])

  return (
    <Container>
      <Header>AFV Show Sensors</Header>
      <Device store={store}/>
      <Table store={store} remotename={remote_name}/>
    </Container>
  )
}