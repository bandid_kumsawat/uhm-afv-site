FROM nginx

# cert
# RUN mkdir -p /etc/nginx/cert/
# COPY ./DigiCertCA.crt /etc/nginx/cert/  
# COPY ./wildcard_moph_go_th.crt /etc/nginx/cert/ 
# COPY ./wildcard_moph_go_th.key /etc/nginx/cert/

# config
# COPY ./nginx.conf /etc/nginx/conf.d/default.conf

# copy html directory
COPY ./build/* /usr/share/nginx/html



# docker build -t nginx/afv . && docker run --name afv-web-app -p 80:8888 -p 443:44300 -d nginx/afv
# sudo docker image rm -f nginx/afv && sudo docker rm -f CONTAINER afv-web-app && sudo docker build -t nginx/afv . && sudo docker run --name afv-web-app -p 80:8888 -p 443:44300 -d nginx/afv 


# sudo docker build -t nginx/afv . && sudo docker run --name afv-web-app -p 80:8888 -p 443:44300 -d nginx/afv


## command using
## sudo docker rm -f CONTAINER afv-web-app && sudo docker image rm -f nginx/afv && sudo docker build -t nginx/afv . && sudo docker run --name afv-web-app -p 8887:80 -p 44433:443 -d nginx/afv